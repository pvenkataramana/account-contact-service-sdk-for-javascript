Feature: Add account contact
  Adds an account contact

  Background:
    Given an addAccountContactReq consists of:
      | attribute                 | validation | type   |
      | accountId                 | required   | string |
      | firstName                 | required   | string |
      | lastName                  | required   | string |
      | phoneNumber               | required   | string |
      | emailAddress              | required   | string |
      | countryIso31661Alpha2Code | required   | string |
      | regionIso31662Code        | required   | string |

  Scenario: Success
    Given I provide a valid addAccountContactReq
    And provide an accessToken identifying me as a partner rep associated with addAccountContactReq.accountId
    When I execute addAccountContact
    Then the account contact is added to the account-contact-service
    And the id is returned