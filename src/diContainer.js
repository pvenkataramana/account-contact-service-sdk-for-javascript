import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import AccountContactServiceSdkConfig from './accountContactServiceSdkConfig';
import AddAccountContactFeature from './addAccountContactFeature';
import GetAccountContactWithIdFeature from './getAccountContactWithIdFeature';
import ListAccountContactsWithAccountIdFeature from './listAccountContactsWithAccountIdFeature';

/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {AccountContactServiceSdkConfig} config
     */
    constructor(config:AccountContactServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(AccountContactServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(AddAccountContactFeature);
        this._container.autoRegister(GetAccountContactWithIdFeature);
        this._container.autoRegister(ListAccountContactsWithAccountIdFeature);

    }

}
